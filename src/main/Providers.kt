/*
 * Copyright (C) 2019  Project swissbib <https://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

data class Providers(val providers: Array<Provider>)
{
    fun getProvider(uri: String): Provider {
        val provider = providers.find {
            val matcher = it.pattern.matcher(uri)
            matcher.matches()
        }
        if (provider == null) {
            throw NoProviderFoundException("Could not find a provider for uri $uri.", uri)
        } else {
            return provider
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Providers

        if (!providers.contentEquals(other.providers)) return false

        return true
    }

    override fun hashCode(): Int {
        return providers.contentHashCode()
    }
}