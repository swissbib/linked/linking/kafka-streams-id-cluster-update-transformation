/*
 * Copyright (C) 2019  Project swissbib <https://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

import org.apache.kafka.streams.KeyValue
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.kstream.Predicate
import org.apache.logging.log4j.Logger
import org.swissbib.SbMetadataModel
import org.swissbib.types.EsMergeActions
import java.util.*

class KafkaTopology(private val properties: Properties, private val log: Logger) {
    private val providers = loadProviders()
    private val clusterBuilder = IdClusterBuilder(providers, log)
    private val parser = JsonParser(providers, log)
    private val elastic = ElasticIndex(clusterBuilder, parser, properties, log)

    fun build(): Topology {
        val to = properties.getProperty("kafka.topic.sink")

        val builder = StreamsBuilder()
        val actionBranches = builder
            .stream<String, SbMetadataModel>(properties.getProperty("kafka.topic.source"))
            .branch(
                Predicate { _, value -> value.esMergeAction == EsMergeActions.RETRY },
                Predicate { _, value -> value.esMergeAction == EsMergeActions.UPDATE },
                Predicate { _, _ -> true }
            )

        // version conflict value
        actionBranches[0]
            .flatMapValues { value -> parser.parseVersionIdCluster(value) }
            .flatMapValues { key, value -> retryCheck(key, value) }
            .to(to)

        // update value
        actionBranches[1]
            // uses flap map to discard parse errors.
            .flatMapValues { readOnlyKey, value -> parser.parse(readOnlyKey, value) }
            .flatMapValues { _, value -> buildCluster(value) }
            // Sets keys to new value!
            .flatMap { key, value -> updateIndex(key, value) }
            .to(to)

        // new value
        actionBranches[2]
            // uses flap map to discard parse errors.
            .flatMapValues { readOnlyKey, value -> parser.parse(readOnlyKey, value) }
            .flatMapValues { _, value -> buildCluster(value) }
            // Sets keys to new value!
            .flatMap { key, value -> checkIndex(key, value) }
            .to(to)

        return builder.build()
    }

    private fun buildCluster(fields: SourceLinks): List<Pair<String, IdCluster>> {
        val cluster = clusterBuilder.build(fields)
        if (cluster == null) {
            log.error("Cluster without VIAF, GND or WIKIDATA ID. Will discard: ${fields.id}")
            return emptyList()
        }
        if (cluster.resources.isEmpty()) {
            log.error("A collection of source links with no known providers: ${fields.id} ${fields.sameAs}")
            return listOf()
        }
        return listOf(Pair(fields.date, cluster))
    }

    private fun checkIndex(key: String, value: Pair<String, IdCluster>): List<KeyValue<String, SbMetadataModel>> {
        return elastic.check(key, value)
    }

    private fun updateIndex(key: String, value: Pair<String, IdCluster>): List<KeyValue<String, SbMetadataModel>> {
        return elastic.update(key, value)
    }


    private fun retryCheck(key: String, value: Pair<String, VersionedIdCluster>): List<SbMetadataModel> {
        return elastic.retry(key, value)
    }
}