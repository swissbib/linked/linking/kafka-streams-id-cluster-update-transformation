/*
 * cluster update transformation
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked

import com.beust.klaxon.Json
import java.util.regex.Pattern
import kotlin.system.exitProcess

data class Provider(
    val provider: String,
    val providerSlug: String,
    val providerNamespace: String,
    val providerIdRegex: String,
    val providerWikidataEntity: String,
    val providerWikidataProperty: String?
) {
    @Json(ignored = true)
    val pattern: Pattern = compilePattern(providerIdRegex)

    private fun compilePattern(source: String): Pattern {
        try {
            return Pattern.compile(source)
        } catch (ex: Exception) {
            println("Was unable to parse pattern for provider $providerSlug.")
            exitProcess(1)
        }
    }

    fun getIdentifier(uri: String): String {
        val match = pattern.matcher(uri)
        if (match.matches()) {
            return match.group("id")
        } else {
            // This should not happen!
            throw IdentifierExtractionFailed("Could not extract identifier from $uri with pattern ${pattern.pattern()} for provider $provider.")
        }
    }

}