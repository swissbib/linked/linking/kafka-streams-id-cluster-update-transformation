/*
 * cluster update transformation
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked

import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Klaxon
import com.beust.klaxon.KlaxonException
import org.apache.logging.log4j.Logger
import org.swissbib.SbMetadataModel
import java.io.StringReader

class JsonParser(private val providers: Providers, private val log: Logger) {

    fun parse(key: String, message: SbMetadataModel): List<SourceLinks> {
        if (message.messageDate == null){
            log.error("Could not parse message with key $key because messageDate is null!")
            return emptyList()
        }
        return when (val parsedMessage = parseObject(message.data)) {
            is JsonObject -> {
                try {
                    val links = parseSameAsJsonObject(parsedMessage)
                    listOf(SourceLinks(key, links, message.esMergeAction, message.messageDate))
                } catch (ex: NoProviderFoundException) {
                    if (ex.isIgnored()) {
                        log.debug(ex.message)
                    } else {
                        log.warn(ex.message)
                    }
                    listOf<SourceLinks>()
                }
            }
            else -> listOf()
        }
    }

    fun parseVersionIdCluster(value: SbMetadataModel): List<Pair<String, VersionedIdCluster>> {
        return try {
            val out = Klaxon().parse<IdCluster>(value.data)
            return when {
                out == null -> listOf()
                value.messageDate == null -> {
                    log.error("Could not parse versioned message ${value.toByteArray("UTF-8")} because there was no message date provided!")
                    listOf()
                }
                else -> {
                    val versionedIdCluster = VersionedIdCluster(value.esDocumentPrimaryTerm, value.esDocumentSeqNum, out)
                    listOf(Pair(value.messageDate, versionedIdCluster))
                }
            }
        } catch (ex: KlaxonException) {
            log.error("JsonParseException: ${ex.message}")
            log.error("Failed messaged: $value.")
            listOf()
        }
    }

    fun parseCluster(value: String): List<IdCluster> {
        return try {
            val out = Klaxon().parse<IdCluster>(value)
            return if (out == null)
                listOf()
            else {
                listOf(out)
            }
        } catch (ex: KlaxonException) {
            log.error("JsonParseException: ${ex.message}")
            log.error("Failed messaged: $value.")
            listOf()
        }
    }

    private fun parseObject(data: String): JsonObject? {
        return try {
            Klaxon().parseJsonObject(StringReader(data))
        } catch (ex: KlaxonException) {
            log.error("JsonParseException: ${ex.message}")
            log.error("Failed messaged: $data.")
            null
        }
    }

    private fun parseSameAsJsonObject(message: JsonObject): List<String> {
        val sameAs = mutableListOf<String>()
        if ("owl:sameAs" in message.keys || "sameAs" in message.keys) {
            when (val data = message["owl:sameAs"] ?: message["sameAs"]) {
                is JsonArray<*> -> {
                    for (item in data) {
                        when (item) {
                            is JsonObject -> {
                                when (val value = item["id"]) {
                                    is String -> sameAs.add(value)
                                    else -> log.error("Could not parse owl:sameAs elements in inner object: $message")
                                }
                            }
                            is String -> sameAs.add(item)
                            else -> log.error("Could not parse owl:sameAs elements in array: $message")
                        }
                    }
                }
                is String -> {
                    sameAs.add(data)
                }
                else -> log.error("Could not parse owl:sameAs elements: $message.")
            }
        }
        return sameAs
    }
}