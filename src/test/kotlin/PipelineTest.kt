/*
 * cluster update transformation
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

import com.beust.klaxon.Klaxon
import org.apache.http.HttpHost
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.kafka.streams.TopologyTestDriver
import org.apache.kafka.streams.test.ConsumerRecordFactory
import org.apache.logging.log4j.LogManager
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest
import org.elasticsearch.action.index.IndexRequest
import org.elasticsearch.action.support.WriteRequest
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestClient
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.client.indices.CreateIndexRequest
import org.elasticsearch.client.indices.GetIndexRequest
import org.elasticsearch.common.xcontent.XContentType
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.swissbib.SbMetadataDeserializer
import org.swissbib.SbMetadataModel
import org.swissbib.SbMetadataSerializer
import org.swissbib.types.EsBulkActions
import org.swissbib.types.EsMergeActions
import java.io.File
import java.nio.charset.Charset


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PipelineTest {
    private val client = RestHighLevelClient(
        RestClient.builder(
            HttpHost("localhost", 9200)
        ))

    private val index = "id-hub-persons-2020-01-02"
    private val log = LogManager.getLogger("IdClusterService")
    private val props = KafkaProperties(log)

    private val testDriver = TopologyTestDriver(KafkaTopology(props.appProperties, log).build(), props.kafkaProperties)

    init {
        if (client.indices().exists(GetIndexRequest(index), RequestOptions.DEFAULT)) {
            client.indices().delete(DeleteIndexRequest(index), RequestOptions.DEFAULT)
        }
        client.indices().create(CreateIndexRequest(index), RequestOptions.DEFAULT)
    }


    private val resourcePath = "src/test/resources/"
    private fun readFile(fileName: String): String {
        return File(resourcePath + fileName).readText(Charset.defaultCharset())
    }

    @AfterAll
    fun removeIndices() {
        client.indices().delete(DeleteIndexRequest("id-hub-persons-2020-01-02"), RequestOptions.DEFAULT)
    }

    @Test
    fun testCase1() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>(StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(
            factory.create(
                props.appProperties.getProperty("kafka.topic.source"), "http://dbpedia.org/resource/Robert_Thomas_(sculptor)",
                SbMetadataModel()
                    .setData(readFile("input1.json"))
                    .setMessageDate("2020-01-02")
                    .setEsIndexName("dbpedia-persons")
                    .setEsMergeAction(EsMergeActions.NEW)
            )
        )
        val output =
            testDriver.readOutput(
                props.appProperties.getProperty("kafka.topic.sink"),
                StringDeserializer(),
                SbMetadataDeserializer()
            )
        assertThrows<java.lang.NullPointerException> {  output.value() }
    }

    @Test
    fun testCase2() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>(StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(
            factory.create(
                props.appProperties.getProperty("kafka.topic.source"), "http://viaf.org/viaf/116475663",
                SbMetadataModel()
                    .setData(readFile("input2.json"))
                    .setMessageDate("2020-01-02")
                    .setEsIndexName("viaf-persons")
                    .setEsMergeAction(EsMergeActions.NEW)
            )
        )
        val output =
            testDriver.readOutput(
                props.appProperties.getProperty("kafka.topic.sink"),
                StringDeserializer(),
                SbMetadataDeserializer()
            )

        val expectedCluster = Klaxon().parse<IdCluster>(readFile("output2.json"))
        val actualCluster = Klaxon().parse<IdCluster>(output.value().data)

        log.info(output.value().data)

        assertAll("test viaf input",
            { assertEquals("https://d-nb.info/gnd/139434003", output.key()) },
            { assertEquals("id-hub-persons-2020-01-02", output.value().esIndexName) },
            { assertEquals(EsBulkActions.CREATE, output.value().esBulkAction) },
            { assertThrows<java.lang.NullPointerException> {  output.value().esDocumentPrimaryTerm } },
            { assertThrows<java.lang.NullPointerException> {  output.value().esDocumentSeqNum } },
            { assert(expectedCluster == actualCluster) }
        )
    }


    @Test
    fun testCase3() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>(StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(
            factory.create(
                props.appProperties.getProperty("kafka.topic.source"), "https://d-nb.info/gnd/13377080X",
                SbMetadataModel()
                    .setData(readFile("input3.json"))
                    .setMessageDate("2020-01-02")
                    .setEsIndexName("gnd-persons")
                    .setEsMergeAction(EsMergeActions.NEW)
            )
        )
        val output =
            testDriver.readOutput(
                props.appProperties.getProperty("kafka.topic.sink"),
                StringDeserializer(),
                SbMetadataDeserializer()
            )

        val expectedCluster = Klaxon().parse<IdCluster>(readFile("output3.json"))
        val actualCluster = Klaxon().parse<IdCluster>(output.value().data)

        log.info(output.value().data)

        assertAll("test gnd input",
            { assertEquals("https://d-nb.info/gnd/13377080X", output.key()) },
            { assertEquals("id-hub-persons-2020-01-02", output.value().esIndexName) },
            { assertEquals(EsBulkActions.CREATE, output.value().esBulkAction) },
            { assertThrows<java.lang.NullPointerException> {  output.value().esDocumentPrimaryTerm } },
            { assertThrows<java.lang.NullPointerException> {  output.value().esDocumentSeqNum } },
            { assertEquals(expectedCluster, actualCluster) }
        )
    }


    @Test
    fun testCase4() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>(StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(
            factory.create(
                props.appProperties.getProperty("kafka.topic.source"), "https://d-nb.info/gnd/13377080X",
                SbMetadataModel()
                    .setData(readFile("input4.1.json"))
                    .setMessageDate("2020-01-02")
                    .setEsIndexName("gnd-persons")
                    .setEsMergeAction(EsMergeActions.NEW)
            )
        )

        val output =
            testDriver.readOutput(
                props.appProperties.getProperty("kafka.topic.sink"),
                StringDeserializer(),
                SbMetadataDeserializer()
            )

        client.index(IndexRequest(output.value().esIndexName).source(output.value().data, XContentType.JSON).id(output.key()), RequestOptions.DEFAULT)
        // Wait for indexed document to actually be visible!
        Thread.sleep(1_000L)

        testDriver.pipeInput(
            factory.create(
                props.appProperties.getProperty("kafka.topic.source"), "http://viaf.org/viaf/69018659",
                SbMetadataModel()
                    .setData(readFile("input4.2.json"))
                    .setMessageDate("2020-01-02")
                    .setEsIndexName("viaf-persons")
                    .setEsMergeAction(EsMergeActions.NEW)
            )
        )

        val output2 =
            testDriver.readOutput(
                props.appProperties.getProperty("kafka.topic.sink"),
                StringDeserializer(),
                SbMetadataDeserializer()
            )
        val expectedCluster = Klaxon().parse<IdCluster>(readFile("output4.1.json"))
        val actualCluster = Klaxon().parse<IdCluster>(output.value().data)
        val expectedCluster2 = Klaxon().parse<IdCluster>(readFile("output4.2.json"))
        val actualCluster2 = Klaxon().parse<IdCluster>(output2.value().data)

        log.info(output.value().data)
        log.info(output2.value().data)

        assertAll("test combined input",
            { assertEquals("https://d-nb.info/gnd/13377080X", output.key()) },
            { assertEquals("id-hub-persons-2020-01-02", output.value().esIndexName) },
            { assertEquals(EsBulkActions.CREATE, output.value().esBulkAction) },
            { assertThrows<java.lang.NullPointerException> {  output.value().esDocumentPrimaryTerm } },
            { assertThrows<java.lang.NullPointerException> {  output.value().esDocumentSeqNum } },
            { assert(expectedCluster == actualCluster) },
            { assertEquals("https://d-nb.info/gnd/13377080X", output2.key()) },
            { assertEquals("id-hub-persons-2020-01-02", output2.value().esIndexName) },
            { assertEquals(EsBulkActions.INDEX, output2.value().esBulkAction) },
            { assertEquals(1,  output2.value().esDocumentPrimaryTerm) },
            { assertEquals(0, output2.value().esDocumentSeqNum) },
            { assertEquals(expectedCluster2, actualCluster2) }
        )
    }


    @Test
    fun testCase5() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>(StringSerializer(), SbMetadataSerializer())
        testDriver.pipeInput(
            factory.create(
                props.appProperties.getProperty("kafka.topic.source"), "http://data.rero.ch/02-A027807323",
                SbMetadataModel()
                    .setData(readFile("input5.json"))
                    .setMessageDate("2020-01-02")
                    .setEsIndexName("rero-viaf-links-next")
                    .setEsMergeAction(EsMergeActions.UPDATE)
            )
        )
        val output =
            testDriver.readOutput(
                props.appProperties.getProperty("kafka.topic.sink"),
                StringDeserializer(),
                SbMetadataDeserializer()
            )
        assertAll("test rero update input (no update)",
            { assertNull(output) }
        )
    }

    @Test
    fun testCase6() {
        val factory = ConsumerRecordFactory<String, SbMetadataModel>(StringSerializer(), SbMetadataSerializer())

        testDriver.pipeInput(
            factory.create(
                props.appProperties.getProperty("kafka.topic.source"), "http://viaf.org/viaf/166393399",
                SbMetadataModel()
                    .setData(readFile("input6.1.json"))
                    .setMessageDate("2020-01-02")
                    .setEsIndexName("viaf-persons-next")
                    .setEsMergeAction(EsMergeActions.NEW)
            )
        )

        val viafOutput =
            testDriver.readOutput(
                props.appProperties.getProperty("kafka.topic.sink"),
                StringDeserializer(),
                SbMetadataDeserializer()
            )



        client.index(IndexRequest(index)
            .id(viafOutput.key())
            .create(true)
            .source(viafOutput.value().data, XContentType.JSON)
            .setRefreshPolicy(WriteRequest.RefreshPolicy.WAIT_UNTIL), RequestOptions.DEFAULT)

        testDriver.pipeInput(
            factory.create(
                props.appProperties.getProperty("kafka.topic.source"), "http://data.rero.ch/02-A011466531",
                SbMetadataModel()
                    .setData(readFile("input6.2.json"))
                    .setMessageDate("2020-01-02")
                    .setEsIndexName("rero-viaf-links-next")
                    .setEsMergeAction(EsMergeActions.UPDATE)
            )
        )
        val output =
            testDriver.readOutput(
                props.appProperties.getProperty("kafka.topic.sink"),
                StringDeserializer(),
                SbMetadataDeserializer()
            )

        val expectedCluster = Klaxon().parse<IdCluster>(readFile("output6.json"))
        val actualCluster = Klaxon().parse<IdCluster>(output.value().data)

        assertAll("test rero input with update",
            { assertEquals("https://d-nb.info/gnd/134944550", output.key()) },
            { assertEquals("id-hub-persons-2020-01-02", output.value().esIndexName) },
            { assertEquals(EsBulkActions.INDEX, output.value().esBulkAction) },
            { assertEquals(expectedCluster, actualCluster)}
        )
    }

}